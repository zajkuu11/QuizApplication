import {QuestionModel} from "./question.model";

export class QuizModel {
  id: number;
  title: string;
  description: string;
  questions: QuestionModel[];
}
