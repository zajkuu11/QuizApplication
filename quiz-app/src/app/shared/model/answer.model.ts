import {QuestionModel} from "./question.model";

export class AnswerModel {
  id: number;
  content: string;
  question: QuestionModel;
}
