import {QuizModel} from "./quiz.model";
import {AnswerModel} from "./answer.model";

export class QuestionModel {
  id: number;
  content: string;
  score: number;
  quiz: QuizModel;
  answers: AnswerModel[] = [];
}
