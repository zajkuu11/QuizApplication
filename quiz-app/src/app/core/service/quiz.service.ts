import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {QuizModel} from "../../shared/model/quiz.model";


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  constructor(private http: HttpClient) { }

  getQuizzes() : Observable<QuizModel[]>{
    return this.http.get<QuizModel[]>('http://localhost:8080/api/quizzes')
  }
}
