import {Component, OnInit} from '@angular/core';
import {QuizModel} from "../../shared/model/quiz.model";
import {ActivatedRoute} from "@angular/router";
import {QuizService} from "../../core/service/quiz.service";

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.sass']
})
export class QuizComponent implements OnInit {

  quizzes: QuizModel[];

  constructor(private quizService: QuizService) {
  }

  ngOnInit(): void {
    this.getCustomers();
  }

  getCustomers() {
    return this.quizService.getQuizzes().subscribe(
      quizzes => {
        console.log(quizzes);
        this.quizzes = quizzes
      }
    )
  }
}
