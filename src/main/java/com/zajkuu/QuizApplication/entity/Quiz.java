package com.zajkuu.QuizApplication.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;


import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "quiz")
public class Quiz {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;

    @OneToMany(
            mappedBy = "quiz",
            cascade = CascadeType.ALL
    )
    @JsonBackReference
    private List<Question> questions = new LinkedList<>();

    public Quiz() {
    }

    public Quiz(String title, String description, List<Question> questions) {
        this.title = title;
        this.description = description;
        this.questions = questions;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    @Override
    public String toString() {
        String result;
        if (questions != null) {
            result = questions.toString();
        }
        result = "Quiz{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description= chuj'" + description + '\'';

        return result;
    }
}
