package com.zajkuu.QuizApplication.converter;

import com.zajkuu.QuizApplication.dto.QuizDto;
import com.zajkuu.QuizApplication.entity.Quiz;

import java.util.stream.Collectors;

public class QuizConverter {
    public static Quiz dtoToEntity(QuizDto quizDto) {
        Quiz quiz = new Quiz(quizDto.getTitle(), quizDto.getDescription(), null);
        quiz.setId(quizDto.getId());
        quiz.setQuestions(quizDto.getQuestionDtos().stream().map(QuestionConverter::dtoToEntity).collect(Collectors.toList()));
        return quiz;
    }

    public static QuizDto entityToDto(Quiz quiz) {
        QuizDto quizDto = new QuizDto(quiz.getId(), quiz.getTitle(), quiz.getDescription(), null);
        quizDto.setQuestionDtos(quiz.getQuestions().stream().map(QuestionConverter::entityToDto).collect(Collectors.toList()));
        return quizDto;
    }
}
