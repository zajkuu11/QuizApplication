package com.zajkuu.QuizApplication.converter;

import com.zajkuu.QuizApplication.dto.QuestionDto;
import com.zajkuu.QuizApplication.entity.Question;

import java.util.stream.Collectors;

public class QuestionConverter {
    public static Question dtoToEntity(QuestionDto questionDto) {
        Question question = new Question(questionDto.getContent(), questionDto.getScore(), questionDto.getQuiz(), null);
        question.setId(questionDto.getId());
        question.setAnswers(questionDto.getAnswerDtos().stream().map(AnswerConverter::dtoToEntity).collect(Collectors.toList()));
        return question;
    }

    public static QuestionDto entityToDto(Question question) {
        QuestionDto questionDto = new QuestionDto(question.getId(), question.getContent(), question.getScore(), question.getQuiz(), null);
        questionDto.setAnswerDtos(question.getAnswers().stream().map(AnswerConverter::entityToDto).collect(Collectors.toList()));
        return questionDto;
    }
}
