package com.zajkuu.QuizApplication.converter;

import com.zajkuu.QuizApplication.dto.AnswerDto;
import com.zajkuu.QuizApplication.entity.Answer;

public class AnswerConverter {
    public static Answer dtoToEntity(AnswerDto answerDto) {
        Answer answer = new Answer(answerDto.getContent(), answerDto.isCorrect(), answerDto.getQuestion());
        answer.setId(answerDto.getId());
        return answer;
    }

    public static AnswerDto entityToDto(Answer answer) {
        AnswerDto answerDto = new AnswerDto(answer.getId(), answer.getContent(), answer.isCorrect(), answer.getQuestion());
        return answerDto;
    }
}
