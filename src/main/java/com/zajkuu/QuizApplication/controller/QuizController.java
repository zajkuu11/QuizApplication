package com.zajkuu.QuizApplication.controller;

import com.zajkuu.QuizApplication.entity.Quiz;
import com.zajkuu.QuizApplication.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping({"/api"})
public class QuizController {

    private QuizService quizService;

    @Autowired
    public void setQuizService(QuizService quizService) {
        this.quizService = quizService;
    }

    @PostMapping
    public Quiz create(@RequestBody Quiz quiz) {
        return quizService.save(quiz);
    }

    @GetMapping(path = {"/quiz/{id}"})
    public Quiz findOne(@PathVariable("id") Long id) {
        System.out.println(quizService.getQuiz(id).getQuestions().get(1));
        return quizService.getQuiz(id);
    }

    @PutMapping
    public Quiz update(@RequestBody Quiz quiz) {
        return quizService.save(quiz);
    }

    @DeleteMapping(path = {"/quiz/delete/{id}"})
    public void delete(@PathVariable("id") Long id) {
        quizService.deleteQuiz(id);
    }

    @GetMapping(path = "/quizzes")
    public List findAll() {
        return quizService.qetQuizList();
    }
}
