package com.zajkuu.QuizApplication.repository;

import com.zajkuu.QuizApplication.entity.Question;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepository extends JpaRepository<Question, Long> {
}
