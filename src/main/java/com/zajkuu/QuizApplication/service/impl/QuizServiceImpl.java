package com.zajkuu.QuizApplication.service.impl;


import com.zajkuu.QuizApplication.entity.Quiz;
import com.zajkuu.QuizApplication.repository.QuizRepository;
import com.zajkuu.QuizApplication.service.QuizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.LinkedList;
import java.util.List;

@Service
public class QuizServiceImpl implements QuizService {

    @Autowired
    private QuizRepository quizRepository;


    @Override
    public List<Quiz> qetQuizList() {
        Iterable<Quiz> quizIterable = quizRepository.findAll();
        List<Quiz> quizList = new LinkedList<>();
        for (Quiz quiz : quizIterable) {
            quizList.add(quiz);
        }
        return quizList;
    }

    @Override
    public Quiz getQuiz(Long id) {
        return quizRepository.findById(id).get();
    }

    @Override
    public Quiz save(Quiz quiz) {
        quizRepository.save(quiz);
        return quiz;
    }

    @Override
    public void deleteQuiz(Long id) {
        quizRepository.deleteById(id);
    }
}
