package com.zajkuu.QuizApplication.service;

import com.zajkuu.QuizApplication.entity.Quiz;

import java.util.List;

public interface QuizService {
    List<Quiz> qetQuizList();

    Quiz getQuiz(Long id);

    Quiz save(Quiz quiz);

    void deleteQuiz(Long id);
}
