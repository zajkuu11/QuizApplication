package com.zajkuu.QuizApplication.dto;
import com.zajkuu.QuizApplication.entity.Quiz;
import java.util.List;

public class QuestionDto {
    private Long id;
    private String content;
    private int score;
    private Quiz quiz;
    private List<AnswerDto> answerDtos;

    public QuestionDto() {
    }

    public QuestionDto(Long id, String content, int score, Quiz quiz, List<AnswerDto> answerDtos) {
        this.id = id;
        this.content = content;
        this.score = score;
        this.quiz = quiz;
        this.answerDtos = answerDtos;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public List<AnswerDto> getAnswerDtos() {
        return answerDtos;
    }

    public void setAnswerDtos(List<AnswerDto> answerDtos) {
        this.answerDtos = answerDtos;
    }
}
