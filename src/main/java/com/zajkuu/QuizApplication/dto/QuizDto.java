package com.zajkuu.QuizApplication.dto;

import java.util.List;

public class QuizDto {
    private Long id;
    private String title;
    private String description;
    private List<QuestionDto> questionDtos;

    public QuizDto(Long id, String title, String description, List<QuestionDto> questionDtos) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.questionDtos = questionDtos;
    }

    public QuizDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<QuestionDto> getQuestionDtos() {
        return questionDtos;
    }

    public void setQuestionDtos(List<QuestionDto> questionDtos) {
        this.questionDtos = questionDtos;
    }


}

