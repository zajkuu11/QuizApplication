package com.zajkuu.QuizApplication.dto;

import com.zajkuu.QuizApplication.entity.Question;

public class AnswerDto {
    private Long id;
    private String content;
    private boolean isCorrect;
    private Question question;

    public AnswerDto() {
    }

    public AnswerDto(Long id, String content, boolean isCorrect, Question question) {
        this.id = id;
        this.content = content;
        this.isCorrect = isCorrect;
        this.question = question;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}
